package main

import c "composite/pkg"

func main() {
	file1 := &c.File{Name: "File 1"}
	file2 := &c.File{Name: "File 2"}
	file3 := &c.File{Name: "File 3"}
	folder1 := &c.Folder{Name: "Folder 1"}
	folder1.Add(file1)
	folder2 := &c.Folder{Name: "Folder 2"}
	folder2.Add(file2)
	folder2.Add(file3)
	folder2.Add(folder1)

	folder2.Search("rose")
}